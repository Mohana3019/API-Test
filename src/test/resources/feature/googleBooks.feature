@reg
Feature: Testing Google Books page

@positive @smoke
Scenario Outline: Validate the Google Books end point and its URL
When Validate get books response code <response_code> 
Then I verify URL '<valid_url>' present

Examples:
|response_code|valid_url|
|200|https://developers.google.com/books/docs/v1/getting_started|

@negative 
Scenario: Validate negative case for Google book endpoint
When Validate get books response code <response_code> 
Then I verify URL '<content>' not present

Examples:
|response_code|content|
|200|https://developers.google.com/books/docs/v1/getting_started|
|200|https://developer.spotify.com/|


