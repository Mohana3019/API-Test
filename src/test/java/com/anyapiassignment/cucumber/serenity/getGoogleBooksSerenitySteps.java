package com.anyapiassignment.cucumber.serenity;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class getGoogleBooksSerenitySteps {

	@Step("Getting all the spotify information")
	public ValidatableResponse getSpotifySteps() {
		return SerenityRest.given().when().get("openapi/spotify_com.v1.openapi.json").then();
	}

	@Step("Getting all google books information")
	public Response getGoogleBooksSteps(String googleBook_Endpoint) {
		return SerenityRest.given().when().get(googleBook_Endpoint);
	}
}
