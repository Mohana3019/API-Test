package com.anyapiassignment.cucumber.stepdefinition;

import com.anyapiassignment.cucumber.serenity.getGoogleBooksSerenitySteps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.hamcrest.CoreMatchers.*;

import org.junit.runner.RunWith;

import com.anyapiassignment.utils.ReuseableSpecifications;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

import org.junit.Assert;

public class googleBooksSteps {

	@Steps
	getGoogleBooksSerenitySteps steps;

	@When("Validate get books response code {int}")
	public void validate_get_books_response_code(Integer responseCode) {
		if (responseCode.equals(200)) {
			steps.getGoogleBooksSteps("openapi/googleapis_com_books.v1.openapi.json").then().statusCode(responseCode);
		} else if (responseCode.equals(404)) {
			steps.getGoogleBooksSteps("openapi/googleapis_com_65454544534").then().statusCode(responseCode);
		}

	}

	@Then("I verify URL {string} present")
	public void i_verify_url_present(String valid_url) {
		steps.getGoogleBooksSteps("openapi/googleapis_com_books.v1.openapi.json").then().statusCode(200)
				.body("externalDocs.url", equalTo(valid_url))
//		.body("externalDocs.url",not(equalTo(("invalid_url"))))	
				.spec(ReuseableSpecifications.getGenericResponseSpec());
	}

	@Then("I verify URL {string} not present")
	public void i_verify_url_not_present(String invalidContent) {
		String responseBody = steps.getGoogleBooksSteps("openapi/googleapis_com_65454544534").then().extract().body().asString();
		Assert.assertFalse(responseBody.contains(invalidContent));
	}

}
